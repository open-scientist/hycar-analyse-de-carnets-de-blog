def run(data):
    import hycar
    from filter import separate_indexes_train_and_set 
    
    indexes_in_set = list(data[data.langue == "fr"][ ((data.crowdsourced_category == "0")  | (data.crowdsourced_category == "1") )].index)

    indexes_separated = separate_indexes_train_and_set.run(indexes_in_set)

    return indexes_separated