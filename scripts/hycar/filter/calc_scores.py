def run(data_sets, mots_valides, signatures_cat):

# Calcul du score pour train & annonces
    data_sets["train_announces"]["score"] = signatures_cat["train_annouces"][mots_valides].T.sum() #mots_valides # mots_stereotypiques

    # remettre mots_valides (sinon "décès" augmente le score)
    # -> mots par type de posts (annonce de parution)

    data_sets["train_announces"]["density_score"] = data_sets["train_announces"]["score"]/data_sets["train_announces"]["length"]

    return data_sets