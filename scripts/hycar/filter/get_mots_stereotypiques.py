def run(crowdsourced_categories):
    mots_stereotypiques = []
    #for item in crowdsourced_categories[crowdsourced_categories.category == "1"].mots_stereotypiques.str.split(",").values:
    for item in crowdsourced_categories[crowdsourced_categories.mots_stereotypiques.notna()].mots_stereotypiques.str.split(",").values:
        mots_stereotypiques += item

    while "" in mots_stereotypiques:
        mots_stereotypiques.remove("")

    mots_stereotypiques = [mot.lower() for mot in mots_stereotypiques]
    mots_stereotypiques = list(set(mots_stereotypiques))

    return mots_stereotypiques