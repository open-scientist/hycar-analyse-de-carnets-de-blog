def run(data, crowdsourced_categories):
    from numpy import nan
    
    categories_equal_to_0 = [category for category in crowdsourced_categories.category.unique() if category not in ["1", nan]]

    for category in categories_equal_to_0:
        data.crowdsourced_category = data.crowdsourced_category.str.replace(category, "0")        

    return data
