
def run(data_sets, store, category_threashold):
    data_sets["test"]["guessed_categories"] = (data_sets["test"].density_score >= category_threashold )\
        .replace(True, "1").replace(False, "0") 

    store["score_algo"].append(
        (data_sets["test"].guessed_categories == data_sets["test"].crowdsourced_category).sum()
            / len(data_sets["test"]) *100 )

    # Score = nombre de catégories données exactement pareil / nombre de posts catégorisés

    for item in [
        ("vrais positifs", "1", "1"),
        ("faux positifs", "0", "1"),
        ("vrais négatifs", "0", "0"),
        ("faux négatifs", "1", "0")
    ]:

        store["vrais_faux"][item[0]].append( (data_sets["test"][data_sets["test"].crowdsourced_category == item[1]].guessed_categories == item[2]).sum() / len(data_sets["test"][data_sets["test"].crowdsourced_category == item[1]]) * 100)

    return (data_sets, store)

    #guesses[j] = data_sets["test"].guessed_categories
