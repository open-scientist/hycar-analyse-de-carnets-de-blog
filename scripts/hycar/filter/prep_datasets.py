def run(data, indexes_separated):
    data_sets = {
        "train": data.loc[indexes_separated["train"]],
        "test": data.loc[indexes_separated["test"]],
    }

    data_sets["train_announces"] = data_sets["train"][data_sets["train"].crowdsourced_category == "1"]
    data_sets["train_original"] = data_sets["train"][data_sets["train"].crowdsourced_category == "0"]	

    return data_sets