def run(signatures_cat, category_threashold, store, mots_valides, data_sets):
    import hycar
    from filter import score

    data_sets["test"]["score"] = signatures_cat["test"][mots_valides].T.sum() 
    data_sets["test"]["density_score"] = data_sets["test"].score / data_sets["test"].length

    interm_res = score.run(data_sets, store, category_threashold)

    return interm_res