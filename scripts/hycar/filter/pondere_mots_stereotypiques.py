def run(data, mots_stereotypiques):
    from nltk import word_tokenize, FreqDist

    data["word_tokenize"] = data[data.crowdsourced_category == "1"].texte.apply(word_tokenize)

    ponderation = {}
    for dico in data[data.crowdsourced_category == "1"].word_tokenize.apply(FreqDist):
        for key in dico.keys():
            if key in mots_stereotypiques:
                if key not in ponderation.keys():
                    ponderation[key] = 0
                ponderation[key] += dico[key]

    sorted_ponderation = sorted(ponderation.items(), key=lambda kv:kv[1] , reverse=True)

    return sorted_ponderation