def run(indexes_in_set):
    from math import floor
    from random import choice
    
    indexes_in_test = list(indexes_in_set)
    indexes_in_train = []
    for i in range(floor(len(indexes_in_set)/2)):
        index = choice(indexes_in_test)
        indexes_in_test.remove(index)
        indexes_in_train.append(index)
    return {
        "train": indexes_in_train,
        "test": indexes_in_test
    }