def run(data, signature, category_threashold):
    # Prédiction

    data["score"] = signature.head(20000).T.sum()

    #Score en densité

    # calcul de la longueur
    data["length"] = data.texte.apply(len)

    data["density_score"] = data.score / data.length

    # Prédiction pour les posts en français
    data["predicted_category"] = (data[data.langue == "fr"].density_score >= category_threashold).replace(True, "1").replace(False, "0") 

    return data