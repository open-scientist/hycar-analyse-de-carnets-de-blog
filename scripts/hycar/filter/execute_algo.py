def run(data, iterations):
    import hycar
    from pandas import DataFrame
    from hycar_config import crowdsourced_categories_file

    from filter import calc_posts_length, load_crowdsourced_categories, get_mots_stereotypiques, create_signature, calc_signature, pondere_mots_stereotypiques, normalize_0_categories, init_scores_store, categorise_signatures, select_stereotypical_words, train, test, prep_indexes_train_and_test, prep_datasets, add_crowdsourced_categories_to_data

    data = calc_posts_length.run(data)

    crowdsourced_categories = load_crowdsourced_categories.run(crowdsourced_categories_file)

    data = add_crowdsourced_categories_to_data.run(data, crowdsourced_categories)

    mots_stereotypiques = get_mots_stereotypiques.run(crowdsourced_categories)

    signature = create_signature.run(data)
    signature = calc_signature.run(data, signature, mots_stereotypiques)

    ponderation = pondere_mots_stereotypiques.run(data, mots_stereotypiques)

    data = normalize_0_categories.run(data, crowdsourced_categories)

    store = init_scores_store.run(data)

    category_threasholds = []

    for j in range(iterations):
        indexes_separated = prep_indexes_train_and_test.run(data)
        data_sets = prep_datasets.run(data, indexes_separated)
        signatures_cat = categorise_signatures.run(signature, indexes_separated, data_sets)

        mots_valides = select_stereotypical_words.run(signatures_cat)
        category_threashold = train.run(signatures_cat, data_sets, mots_valides) 
        store["category_threashold"].append(category_threashold)

        interm_res = test.run(signatures_cat, category_threashold, store, mots_valides, data_sets)

    final_data_sets = interm_res[0]
    store = interm_res[1]

    mean_score = DataFrame(store["score_algo"]).mean().loc[0]
    
    #guesses[j] = data_test.guessed_categories

    vrais_faux_mean = {}
    for item in [
            ("vrais positifs", "1", "1"),
            ("faux positifs", "0", "1"),
            ("vrais négatifs", "0", "0"),
            ("faux négatifs", "1", "0")
        ]:
        vrais_faux_mean[item[0]] = DataFrame(store["vrais_faux"][item[0]]).mean().loc[0]

    return {
        "category_threashold": DataFrame(store["category_threashold"]).mean().loc[0],
        "mean_score": mean_score,
        "vrais_faux_mean": vrais_faux_mean,
        "data_sets": data_sets
    }