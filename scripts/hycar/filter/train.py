def run(signatures_cat, data_sets, mots_valides):
    import hycar
    from filter import calc_scores
    from filter import extract_category_threashold

    data_sets = calc_scores.run(data_sets, mots_valides, signatures_cat)
    category_threashold = extract_category_threashold.run(data_sets)
    
    return category_threashold