def run(data, signature, mots_stereotypiques):
    for mot in mots_stereotypiques:
        signature[mot] = data.texte.str. match(".*{}.*".format(mot))
    return signature