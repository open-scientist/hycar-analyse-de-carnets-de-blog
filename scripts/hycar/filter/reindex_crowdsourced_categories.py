def run(crowdsourced_categories):
    crowdsourced_categories["real_index"] = crowdsourced_categories["index"] - 1
    crowdsourced_categories.set_index("real_index", inplace=True)

    return crowdsourced_categories