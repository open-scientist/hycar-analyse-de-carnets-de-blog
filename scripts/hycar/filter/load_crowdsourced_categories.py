def run(crowdsourced_categories_file):
    from json import loads
    from pandas import read_csv
    import hycar
    from filter import reindex_crowdsourced_categories

    description = loads(open(crowdsourced_categories_file, 'r').read())

    crowdsourced_categories = read_csv(
        description["path"],
        sep=description["separator"],
        names=description["columns_names"]
        )

    crowdsourced_categories = reindex_crowdsourced_categories.run(crowdsourced_categories)
    
    return crowdsourced_categories