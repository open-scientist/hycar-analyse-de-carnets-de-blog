# Étude des signatures des posts catégorisés annonces

def run(signature, indexes_separated, data_sets):
    signatures_cat = {
        "train_annouces": signature.loc[data_sets["train_announces"].index],
        "all_categorisees": signature.loc[indexes_separated["train"] + indexes_separated["test"]],
        "test": signature.loc[indexes_separated["test"]]
    }

    return signatures_cat

    #signature_train_annouces = signature_ponderee.loc[data_sets["train_announces"].index]
    #signatures_categorisees = signature_ponderee.loc[indexes_in_train + indexes_in_test]