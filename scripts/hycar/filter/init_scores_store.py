def run(data):
    from pandas import DataFrame
    store = {
        "category_threashold": [],
        "score_algo": [],
        "guesses": DataFrame(index=data.index),
        "vrais_faux": {
            "vrais positifs": [],
            "faux positifs": [],
            "vrais négatifs": [],
            "faux négatifs": []
        }
    }

    return store