# Extraction d une valeur plausible de seuil
def run(data_sets):
    category_threashold = data_sets["train_announces"][data_sets["train_announces"].density_score > 0.0001 ].density_score.min()
    return category_threashold
