def run(data):
    """Calcule la longueur des posts"""
    data["length"] = data.texte.apply(len)
    return data