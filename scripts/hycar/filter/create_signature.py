def run(data):
    from pandas import DataFrame
    
    signature = DataFrame(data.index)
    signature.drop(0, axis=1, inplace=True)
    return signature