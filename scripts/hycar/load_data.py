from pandas import read_csv, to_datetime
from json import loads

def load(data_description_file):
    description = loads(open(data_description_file, 'r').read())[0]

    data_with_failing = read_csv(
                            description["filename"],
                            sep=description["sep"],
                            names=description["columns"]
                        )

    data = __remove_failing__(data_with_failing)
    data.date = to_datetime(data.date)
    return data

def __remove_failing__(data): # Ceci fait partie du nettoyage des données, a posteriori
    indexes_to_drop = [174906] + list(data.date[data.date.str.match("1000-02-11")].index)
    data.drop(indexes_to_drop, inplace=True)
    return data