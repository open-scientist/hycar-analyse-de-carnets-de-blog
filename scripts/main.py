
from scripts.hycar.prepare import prepare
from scripts.hycar.make_recommendations import make_recommendations
from scripts.hycar.filter_announces import filter_announces
from scripts.hycar.output_recommendations import output_recommendations
from scripts.hycar.push_results_remote import push_results_remote

if __name__ == "__main__":

    prepared_data = prepare.prepare()

    filtered_data = filter_announces.filter_announces(prepared_data)

    recommendations = make_recommendations.make_recommendations(filtered_data)

    output_recommendations.output_recommendations(recommendations)

    push_results_remote.push_results_remote(recommendations)