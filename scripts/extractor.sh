offset=0

#####Ne fonctionne pas assez vite
xpaths='/doc/str[@name="naked_titre"]','/doc/str[@name="naked_texte"]','/doc/str[@name="site_name"]','/doc/date[@name="datepubli"]','/doc/str[@name="url"]','/doc/str[@name="autodetect_lang"]'

while [[ $offset -le 100 ]];do
  (
  echo $xpaths > extracted/extracted-$offset.csv;

  i=$((offset*3443))
    while [[ $i -le $(($i+3443)) ]];do
    for xpath in $(echo $xpaths|sed 's_,_ _g'); do
      echo $(cat hypo_texte-$i.xml|xmllint --xpath "$xpath" -|sed 's_^<arr name="contributeur\_auteur">__;s_</arr>$__'|sed '{:a;N;$!ba;s_\n *_\;_g}'|sed 's_<date name="datepubli">__;s_</date>__'|sed 's_ *<str name=[^>]\+>__;s_</str>__'|sed 's_,_ %2C_g;s_\;_%3b_g')"," ;
    done|sed '{:a;N;$!ba;s_\n__g}' >> extracted/extracted-$offset.csv;
    i=$((i+1))
  done &
  ) &
  offset=$((offset+1))
done
## fin du pas assez vite

# récupère les fichiers avec un post par fichier
for i in 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19;do echo $i; sed -i '{:a;N;$!ba;s_\n *__g}' cp hypo_texte-$i*.xml split-on-one-line/. ;done

cd split-on-one-line

# met l'ensemble de chaque post sur une seule ligne
for i in 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19;do echo $i; sed -i '{:a;N;$!ba;s_\n *__g}' hypo_texte-$i*.xml |sed -i 's_$_\n_' hypo_texte-$i*.xml ;done

# Identifie les posts sans texte
for i in 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19;do                                                                  grep -v  "\<naked_texte\>" hypo_texte-$i*.xml |sed 's_^hypo\_texte-\([^\.]\+\).xml\(.*\)$_\1_';done > /home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/confidentiel/interm/posts-sans-texte.csv

# Exclue les posts sans texte
# Il y a 2942 posts sans texte
mkdir posts-sans-texte;for index in $(cat /home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/confidentiel/interm/posts-sans-texte.csv);do                                                  mv hypo_texte-$index.xml posts-sans-texte/.    ;done

#Identifie les posts sans titre
for i in 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19;do                                                                  grep -v  "\<naked_titre\>" hypo_texte-$i*.xml |sed 's_^hypo\_texte-\([^\.]\+\).xml\(.*\)$_\1_';done > /home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/confidentiel/interm/posts-sans-titre.csv

#Exclue les posts sans titre
mkdir posts-sans-titre;for index in $(cat /home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/confidentiel/interm/posts-sans-titre.csv); do                                                mv hypo_texte-$index.xml posts-sans-titre/.;done

# Tous les posts ont une collection d'auteur·e·s
 for i in 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19;do                                                                  grep -v -P  "name=\"contributeur\_auteur\"\>" hypo_texte-$i*.xml |sed 's_^ypo\_texte-\([^\.]\+\).xml\(.*\)$_\1_';done|less

# de même pour site_name, <date name="datepubli">, <str name="url">

#Identification des posts sans langue détectée
for i in 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19;do                                                                  grep -v -P  "\<str name=\"autodetect_lang\"\>" hypo_texte-$i*.xml |sed 's_^hypo\_texte-\([^\.]\+\).xml\(.*\)$_\1_';done > /home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/confidentiel/interm/posts-sans-langue.csv

#Exclusion des posts sans la langue
mkdir posts-sans-langue;for index in $(cat /home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/confidentiel/interm/posts-sans-langue.csv);do mv hypo_texte-$index.xml posts-sans-langue/.;done

mkdir sedified
offset=0
while [[ $offset -le 100 ]];do
  (
  i=$((offset*3443))
    while [[ $i -le $(($i+3443)) ]];do
        cat hypo_texte-$i.xml 2>> /tmp/err|\
            sed 's_\;_%3b_g;s_^.*<doc>__;s_<\/str><\/doc>__;s_<str name="naked\_titre">__;s_</str><arr name="contributeur\_auteur">_\;_;s_</arr><str name="naked\_texte">_\;_;s_</str><str name="site\_name">_\;_;s_</str><date name="datepubli">_\;_;s_</date><str name="url">_\;_;s_</str><str name="autodetect\_lang">_\;_;s_</str></doc>$_\n_' > sedified/hypo_texte-$i.csv;
    i=$((i+1))
  done &
  ) &
  offset=$((offset+1))
done

mkdir sedified/compiled
offset=3443
start=0
while [[ $start -le 344247 ]];do
  (
  i=$start
    while [[ $i -le $((start+3443)) ]];do
    cat sedified/hypo_texte-$i.csv >> sedified/compiled/hypo_texte-$start-and-next.csv
    i=$((i+1))
  done &
  ) &
  start=$((start+offset))
done

mkdir sedified/compiled/unified
cat sedified/compiled/*.csv > sedified/compiled/unified/unified.csv ;sed -i '/^$/d' sedified/compiled/unified/unified.csv

# Identifie les lignes ou l'extraction n'a pas marché et les mets de côté
mkdir sedified/compiled/unified/failed/
sed -n '/naked_texte/p' sedified/compiled/unified/unified.csv > sedified/compiled/unified/failed/failed-naked_texte.csv

# Exclue les lignes où l'extraction n'a pas marché
sed  '/naked_texte/d' sedified/compiled/unified/unified.csv

# Identifie et isole les lignes où il y a bien 7 champs
#sed -n '/^[^\;]*\;[^\;]*\;[^\;]*\;[^\;]*\;[^\;]*\;[^\;]*\;/p' sedified/compiled/unified/unified.csv > sedified/compiled/unified/unified-7fields.csv

# Exclue les lignes où il n'y a pas 7 champs
#sed -i '/^[^\;]*\;[^\;]*\;[^\;]*\;[^\;]*\;[^\;]*\;[^\;]*\;[^\;]*$/d' sedified/compiled/unified/unified.csv





# See duplicated crowdsourcing
cut -d ';' -f1 /home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/interm/crowdsourced_categories_and_stereotypical_words.csv | sort |uniq -d |less














#Next is in notebooks

# Now, in Python

# Ceci est la fonction à exécuter au début de chaque script
import pandas

names=["titre","auteur","texte","blog","date","url","langue"]
filename="/home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/confidentiel/interm/Urfist_Hypotheses/split/split-on-one-line/sedified/compiled/unified/unified.csv"
sep="\;"
data=pandas.read_csv(filename, sep=sep, names=names)
data.drop([174906], inplace=True)
data.drop(data.date[data.date.str.match("1000-02-11")].index, inplace=True)
data.date = pandas.to_datetime(data.date)


# La suite, ce sont des scripts/notebooks séparés

# Langues
# Identification des langues présentes
data.langue.unique()

# Nombre de posts par langue
nombre_de_posts_par_langue = {} 
 for langue in data.langue.unique(): 
     nombre_de_posts_par_langue[langue] = (data.langue == langue).sum()  

from operator import itemgetter
sorted_nombre_de_posts_par_langue = sorted(nombre_de_posts_par_langue.items(), key=itemgetter(1), reverse=True)

# Sélection des langues les plus présentes
threashold_language = 3000
sorted_nombre_de_posts_par_langue_threasholded = [x for x in sorted(nombre_de_posts_par_langue.items(), key=operator.itemgetter(1), reverse=True) if x[1] > threashold_language ]

# Nombre de posts dans les langues les plus présentes
total_threasholded = 0 
for item in sorted_nombre_de_posts_par_langue_threasholded: 
    total_threasholded += item[1] 
print(total_threasholded) 



# total = 336714
# sur  len(data) = 338666 (1952 de mis de côté)

# Langues au-dessus du threashold
langues_au_dessus_threashold = [item[0] for item in sorted_nombre_de_posts_par_langue_threasholded] 


# TABLE PAR LANGUE





# Auteurs

# Recherche d'auteurs multiples 
data[data.auteurs.str.match("</str><str>")]
# aucun !

# Nettoie le champ auteurs
data.auteurs=data.auteurs.str.replace("^<str>", "").str.replace("</str>$", "")

# Liste des auteur·e·s
liste_des_auteurs = data.auteurs.unique()

# Nombre total d'auteurs :
len(liste_des_auteurs)

# Nombre de posts par auteur
nombre_de_posts_par_auteur = data.groupby("auteurs").count().titre.sort_values(ascending=False)

# Langues des posts de chaque auteur
langue_par_auteur = {}
 for auteur in nombre_de_posts_par_auteur.index:
     langue_par_auteur[auteur] = data.langue[data.auteurs == auteur].unique()

# Liste des auteurs mono-lingues

# Liste des auteurs EN+ une autre langue

# Liste des auteur >=3 langues



# Par site

liste_blogs_urls = data.site.unique()

# Nombre de blogs
    len(liste_blogs_urls)
#2994

# Nombre de posts par blog
nombre_de_posts_par_blog = data.groupby("site").count().titre.sort_values(ascending=False)

# Sur le blog avec le plus de posts, il y a 269 auteur·ices
len(data.auteurs[data.site == nombre_de_posts_par_blog.index[0] ].unique())

# Blogs et langues

# Blogs nombre d'auteur·ices



# TABLE PAR AUTEUR










# Il y a un auteur qui a écrit avant 1800
data[data.date <= pandas.to_datetime("1800" + "-01-01 00:00:00+00:00")].auteur

# Par date de publication


# Identification de la publication qui est avant 1900
data[data.date.str.match("1498")].index + data.date[data.date.str.match("1000-02-11")].index
# Suppression de la ligne 174906
data.drop([174906], inplace=True) 
# idem pour l'un 1000
data.drop(, inplace=True)

pandas.to_datetime(data.date_publication).sort_values(ascending=False)






# Par urls

len(data.url.unique())
# 338567

len(data.url)
#338663

# Il y a quelques urls qui sont présentes plusieurs fois ???

# Certains posts sont présents deux fois
    for url in data[data.url.duplicated()].url:
        print(url, (data.url == url).sum())

# Heureusement, ces posts ont vraiment exactement les mêmes textes, donc on va pouvoir les supprimer
 for url in data[data.url.duplicated()].url: 
     a= data.texte[data.url == url] 
     if not a.iloc[0] == a.iloc[1]: 
         print(a) 

data.drop_duplicates("url", inplace=True)






# Par site et par date

# Quelques posts sont publiés à exactement la même date, mais c'est anecdotique
data.groupby(["site", "date"]).count().titre.sort_values(ascending=False)

(data.groupby(["blog", "date"]).count().titre.sort_values(ascending=False) >= 2).sum()
#Il y en a 60 pour lesquels il y a 3 posts à la même date ou plus, et 381 2 ou plus

# Le mois avec le plus de publications
data.set_index("date").resample('M').count().titre.sort_values(ascending=False)

# Ré-échantillonnage par mois
par_mois = data.set_index("date").resample('M').count().titre
# Plot des mois avec le plus de publications
par_mois[par_mois > 50].plot.bar()

# On voit une augmentation du nombre de publications au fil des années, puis une légère baisse (année pas finie ?)
# Légère baisse en 2018, puis année 2019 pas terminée



# On voit une périodicité dans la publication par mois !


# Nombre d'auteurs par blog

data[["blog", "auteur"]].drop_duplicates().pivot_table(index=['blog'], aggfunc='size').sort_values(ascending=False)

# Nombre de blogs par auteur
data[["blog", "auteur"]].drop_duplicates().pivot_table(index=['auteur'], aggfunc='size').sort_values(ascending=False)


# Cela nous permet de repérer un auteur du nom de "Rédaction". Il n'est pas possible de savoir si c'est un auteur unique

# Nombre d'auteurs ayant écrit sur un ou plusieurs blogs
 (data[["blog", "auteur"]].drop_duplicates().pivot_table(index=['auteur'], aggfunc='size').sort_values(ascending=False) >= 2).sum()
# 1259 sur 2 blogs ou plus
# 386 sur 3 blogs ou plus
# 15 sur 10 blogs ou plus

# Question : quelle part de ceux-là sont des homonymes ?

# Les auteurs de multiples blogs écrivent principalement en allemand et en français
data.set_index("auteur")[data[["blog", "auteur"]].drop_duplicates().pivot_table(index=['auteur'], aggfunc='size').sort_values(ascending=False) > 5].pivot_table(index=['langue'], aggfunc='size').sort_values(ascending=False)

# à l'inverse, les blogs avec plusieurs auteurs sont principalement français, puis allemand
data.set_index("blog")[data[["blog", "auteur"]].drop_duplicates().pivot_table(index=['blog'], aggfunc='size').sort_values(ascending=False) > 5].pivot_table(index=['langue'], aggfunc='size').sort_values(ascending=False)







# Étudions comment les auteurs sont monolingues ou bilingues

data[["auteur", "date", "langue"]].set_index("date").pivot_table(index=["auteur", "langue"], aggfunc='size').sort_values(ascending=False).head(20)

# [PROFILE]
# Danièle Revel semble écrire en deux langues !

# Danièle Revel écrit sur un seul blog
data[["auteur", "date", "langue", "blog"]].set_index("date").pivot_table(index=["auteur", "langue", "blog"], aggfunc='size').sort_values(ascending=False).head(20)

# Parmi les 60 auteur·es les plus prolifiques, elle est la seule à le faire



# Étudions comment l'auteur "François" publie dans le temps 
    data[["auteur", "date", "blog"]].set_index(["auteur", "date"]).loc["François"].resample("M").count().plot.bar()
# C'est clairement par saisons






# On s'attaque au texte

data["texte_word_tokenize"] = data.texte.apply(nltk.word_tokenize)

#Et aussi aux titres
data["titre_word_tokenize"] = data.titre.apply(nltk.word_tokenize)



# Catégorisation manuelle de posts

# Maintenant, à partir des posts avec un score haut

#for index in $(python3 -c 'from random import randint;print(" ".join([ str(randint(0,344247)) for x in range(100) ]))');do clear ;line=$(sed -n $index'p' "/home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/confidentiel/interm/Urfist_Hypotheses/split/split-on-one-line/sedified/compiled/unified/unified.csv");  titre=$(echo $line | awk -F ';' '{print $1}') ; texte=$(echo $line | awk -F ';' '{print $3}');echo "$titre";echo ;echo "*********************";echo "\n";echo "$texte";echo  ; echo "*********************"; echo "Catégorie ?" ; read category; echo "mots stéréotypiques ?";read stereotipical_words; echo $index";"$category";"$stereotypical_words >>crowdsourced_categories_and_stereotypical_words.csv;  done

# Extraction des infos crowdsourcées
crowdsourced_categories = pandas.read_csv("/home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/interm/crowdsourced_categories_and_stereotypical_words.csv", sep=";", names=["index", "category", "mots_stereotypiques"])

crowdsourced_categories["real_index"] = crowdsourced_categories["index"] -1
crowdsourced_categories.set_index("real_index", inplace=True)

data["crowdsourced_category"] = crowdsourced_categories.category 

mots_stereotypiques = []
#for item in crowdsourced_categories[crowdsourced_categories.category == "1"].mots_stereotypiques.str.split(",").values:
for item in crowdsourced_categories.mots_stereotypiques.str.split(",").values:
    try:
        mots_stereotypiques += item
    except:
        pass
#data[data.crowdsourced_category == "1"].mots_stereotypiques = list(set(mots_stereotypiques)) #.remove("")

mots_stereotypiques = [mot.lower() for mot in mots_stereotypiques]

while "" in mots_stereotypiques:
    mots_stereotypiques.remove("")

mots_stereotypiques = list(set(mots_stereotypiques))

# Étude des textes
signature = pandas.DataFrame(data.index)
signature.drop(0, axis=1, inplace=True)

import re

for mot in mots_stereotypiques:
    signature[mot] = data.head(10000).texte.str. match(".*{}.*".format(mot)) #count(mot, re.IGNORECASE) #match(".*{}.*".format(mot))
# Le taux de faux positifs passe de 63 à 28% quand on revient aux matchs (signature) plutôt que count (aggrégation)

# Nombre d'occurences par mot

for mot in mots_stereotypiques:
    print(mot, signature.head(10000)[mot].sum())


# Pondération des mots

#Tokenization
data["word_tokenize"] = data[data.crowdsourced_category == "1"].texte.apply(nltk.word_tokenize)

# Récupération de la distribution
ponderation = {}
for dico in data[data.crowdsourced_category == "1"].word_tokenize.apply(nltk.FreqDist):
    for key in dico.keys():
        if key in mots_stereotypiques:
            if key not in ponderation.keys():
                ponderation[key] = 0
            ponderation[key] += dico[key]



# Pondération
sorted_ponderation = sorted(ponderation.items(), key=lambda kv:kv[1] , reverse=True)





# Prise en compte de la pondération par mots stéréotypique

signature_ponderee = pandas.DataFrame(index=signature.index)
for column in signature.columns:
       try:
            signature_ponderee[column] = signature[column] * ponderation[column]
       except:
           signature_ponderee[column] = signature[column]
#	   .replace(True, 1).replace(False, 0)

















# Ceci n'est pas dans le notebook


# Score absolu
# retrait de l'indice inutile

data["score"] = signature.head(10000).T.sum()

#Score en dentisé

# calcul de la longueur
data["length"] = data.texte.apply(len)

data["density_score"] = data.score / data.length

# Travail manuel effectué après avoir mis un seuil assez bas : catégoriser les posts qui ont été identifiés. Cela va permettre de remonter le score
# export des posts catégorisés annonces :
category_threashold = 0.8

indexes_to_categorize = [x for x in data.head(1000)[(data["density_score"].head(1000) * 1000 > category_threashold)].index if x not in list(crowdsourced_categories["index"])]
random.shuffle(indexes_to_categorize)
open("/tmp/categorized-announcements", "w").write(str(indexes_to_categorize)) 

# Catégorisation à la main de ces posts
for index in $(cat "/tmp/categorized-announcements"| sed 's_\[__g;s_\]__g;s_,__g'|less);do clear; sed -n $index'p' "/home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/confidentiel/interm/Urfist_Hypotheses/split/split-on-one-line/sedified/compiled/unified/unified.csv"; read category; echo $index";"$category";" >> /home/u/abc/factory/hycar-analyse-de-carnets-de-blog/data/interm/crowdsourced_categories_and_stereotypical_words.csv ;done

# à partir des posts catégorisés annonces, on a une petite liste d'auteurs
list(data[(data["crowdsourced_category"] == "1")].auteur.unique())

# Classification : bonne pour les petites annonces d'événements (50aine de mots)

# Au-delà d'une certaine longueur du post : beaucoup de faux positifs

#[TODO] filtrer uniquement pour le français pour les posts d'annonces

# Dans les posts catégorisés comme annonces, étude de la distribution
import string
extended_punctuation = ["''", '_0x446d', '’', '``', 'A�', '–', '||', '«', '\\-|', '»', '==', '|i\\-', '“', '”', '3b-']
from nltk.corpus import stopwords
extended_stopwords_french = ['les', 'plus', '3b', '_0xecfdx1', 'La', 'la', 'lt', 'comme', '_0xecfdx2']

res = []
for item in data[data.word_tokenize.notna()].word_tokenize.to_list():
    res += item

stop_words = [x for x in string.punctuation] + extended_punctuation + [str(x) for x in range(100)] + stopwords.words("french") + extended_stopwords_french + stopwords.words("english") + stopwords.words("german")

res_no_stopwords = [item for item in res if item not in stop_words]

dist = nltk.FreqDist(res_no_stopwords)


# Cela nous donne une signature des mots les plus courants
dist.most_common()[:40]

# Qui est à comparer avec la signature pour les posts qui ne sont pas des annonces.


# Nettoyage : il y a de l'hexadécimal (\\\x5F)
(data.head(1000).texte.str.match(".*\\\\x..")).sum()
# nettoyage
data.texte = data.texte.str.replace("\\\\x..", "")
(data.head(1000).texte.str.match(".*\\\\x..")).sum()
# good

#application de la tokenization






















# Train & test
data.crowdsourced_category = data.crowdsourced_category.str.replace("billet original", "0")        

# Ça donne 225 points (now 509)
((data.crowdsourced_category == "0")  | (data.crowdsourced_category == "1")).sum()

# Index des lignes dans le jeux de données curées
data[(data.crowdsourced_category == "0")  | (data.crowdsourced_category == "1")].index

import random
import math

score_algo = []
guesses = pandas.DataFrame(index=data.index)
vrais_faux = {"vrais positifs": [], "faux positifs": [], "vrais négatifs": [], "faux négatifs": []}
for j in range(100):

	indexes_in_set = list(data[data.langue == "fr"][ ((data.crowdsourced_category == "0")  | (data.crowdsourced_category == "1") )].index)  
	indexes_in_test = list(indexes_in_set)
	indexes_in_train = []
	for i in range(math.floor(len(indexes_in_set)/2)):
	    index = random.choice(indexes_in_test)
	    indexes_in_test.remove(index)
	    indexes_in_train.append(index)

	data_train = data.loc[indexes_in_train]

	data_test = data.loc[indexes_in_test]

	# Dans le jeu d'entrainement, il y a 30% d'annonces
	(data.loc[indexes_in_train].crowdsourced_category == "1").sum() / len(indexes_in_train)

	# Dans le train : sélectionner uniquement les annonces (1)

	data_train_announces = data_train[data_train.crowdsourced_category == "1"]

	data_train_original = data_train[data_train.crowdsourced_category == "0"]	

	# Étude des signatures des posts catégorisés annonces

	signature_train_annouces = signature.loc[data_train_announces.index]
	#signature_train_annouces = signature_ponderee.loc[data_train_announces.index]

	# Sélection des mots effectivement repérés

	signatures_categorisees = signature.loc[indexes_in_train + indexes_in_test]
	#signatures_categorisees = signature_ponderee.loc[indexes_in_train + indexes_in_test]

	mots_valides = list(signatures_categorisees.T[signatures_categorisees.sum() >=1.0].T.columns)

	#Retrait des mots repérés dans des posts non annonces
	#TODO

	# Recalcul du score pour train & annonces
	data_train_announces.score = signature_train_annouces[mots_valides].T.sum() #mots_valides # mots_stereotypiques

	# remettre mots_valides (sinon "décès" augmente le score)
	# -> mots par type de posts (annonce de parution)

	data_train_announces["density_score"] = data_train_announces["score"]/data_train_announces["length"]


	# Extraction d une valeur plausible de seuil
	category_threashold = data_train_announces[data_train_announces.density_score > 0.0001 ].density_score.min()

	# Catégoriser le test set

	signature_test = signature.loc[indexes_in_test]
	#signature_test = signature_ponderee.loc[indexes_in_test]

	data_test["score"] = signature_test[mots_valides].T.sum() 

	data_test["density_score"] = data_test.score / data_test.length


	#Score

	data_test["guessed_categories"] = (data_test.density_score >= category_threashold ).replace(True, "1").replace(False, "0") 

	# Score = nombre de catégories données exactement pareil / nombre de posts catégorisés
	
	score_algo.append( (data_test.guessed_categories == data_test.crowdsourced_category).sum() / len(data_test) *100 )

	vrais_faux["vrais positifs"].append((data_test[data_test.crowdsourced_category == "1"].guessed_categories == "1").sum() / len(data_test[data_test.crowdsourced_category == "1"]) *100)
        # 	
	vrais_faux["faux positifs"].append((data_test[data_test.crowdsourced_category == "0"].guessed_categories == "1").sum() / len(data_test[data_test.crowdsourced_category == "0"]) *100)

        #Vrais négatifs
	vrais_faux["vrais négatifs"].append((data_test[data_test.crowdsourced_category == "0"].guessed_categories == "0").sum() / len(data_test[data_test.crowdsourced_category == "0"]) *100)

	#Faux négatifs
	vrais_faux["faux négatifs"].append((data_test[data_test.crowdsourced_category == "1"].guessed_categories == "0").sum() / len(data_test[data_test.crowdsourced_category == "1"]) *100)



	guesses[j] = data_test.guessed_categories

# score_algo
print("score de l'algo", pandas.DataFrame(score_algo).mean())

# Vrais positif
print("vrais positifs", (data_test[data_test.crowdsourced_category == "1"].guessed_categories == "1").sum() / len(data_test[data_test.crowdsourced_category == "1"]) *100)

#Faux positifs
print("faux positifs", (data_test[data_test.crowdsourced_category == "0"].guessed_categories == "1").sum() / len(data_test[data_test.crowdsourced_category == "0"]) *100)

#Vrais négatifs
print("vrais négatifs", (data_test[data_test.crowdsourced_category == "0"].guessed_categories == "0").sum() / len(data_test[data_test.crowdsourced_category == "0"]) *100)

#Faux négatifs
print("faux négatifs", (data_test[data_test.crowdsourced_category == "1"].guessed_categories == "0").sum() / len(data_test[data_test.crowdsourced_category == "1"]) *100)

# 

# Évaluation de la stabilité de l'algo
[ ( (guesses.loc[i] == "0").sum(), (guesses.loc[i] == "1").sum() ) for i in range (len(data_test))]






# Prédiction

data["score"] = signature.T.sum()

#Score en dentisé

# calcul de la longueur
data["length"] = data.texte.apply(len)

data["density_score"] = data.score / data.length

# Prédiction pour les posts en français
data["predicted_category"] = (data[data.langue == "fr"].density_score >= category_threashold).replace(True, "1").replace(False, "0") 

data_sans_annonces = data[data["predicted_category"] = "0"]
