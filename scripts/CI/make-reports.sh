for line in $(cat reports/reports-list.csv|sed 's_ _%20_g' | tail -n+2);do
  variable="$(echo $line | sed 's_"__g' | sed 's_%20_ _g;s_,.*__')";
  value=$(echo $line | sed 's_%20_ _g;s_.*,__');
  filename=reports/badges/$(echo $variable|sed 's_ _-_g').svg
  badge "$variable" "$value" :blue .svg > $filename
done

