<!-- Badges :
git & gitlab (base : .gitignore app notebooks data scripts)
gitlab-page à partir du README only : Page : https://open-scientist.gitlab.io/hycar-analyse-de-carnets-de-blog/index.html
Couverture tests,
Binder,
CI,
Review du code/Audit,
Beta-testing&profils,
Architecture/Modularité,
Roadmap&pondération,
**Missions de contribution**
**visio**, livecoding
**historique du code (commits)**
-->


# HYCAR - analyse de carnets de blog

## Exécuter les scripts

Lancer une instance de Jupyter lab où exécuter les scrpits en cliquant sur l'icône suivante : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fhycar-analyse-de-carnets-de-blog/master?urlpath=lab%2Ftree%2Fmain.ipynb)

## Données

Les données sont placées dans un [module dédié](https://gitlab.com/open-scientist/hycar-data/).
